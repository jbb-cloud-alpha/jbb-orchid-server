FROM tomcat:8.5.16-jre8
RUN rm -rf /usr/local/tomcat/webapps/ROOT
COPY /target/*.war /usr/local/tomcat/webapps/ROOT.war
CMD ["catalina.sh", "run"]
EXPOSE 8080